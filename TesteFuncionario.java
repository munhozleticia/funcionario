//Letícia Munhoz
//Exercício apostila Caelum 1-5

import java.util.*;

public class TesteFuncionario {
	
	public static void main(String[] args) {
	
		Funcionario f1=new Funcionario ();
	
		f1.nome="Anacleto";
		f1.departamento="Financeiro";
		f1.salario=5000000;
		f1.dataEntrada = "21/09/2000";
		f1.rg = "12345678";
		f1.recebeAumento(20);
		
		
		
		Funcionario f2=new Funcionario ();
		f2 = f1; 
		
		
		if (f1==f2){
			System.out.println("Iguais");
			f1.mostra();
		}
		else {
			System.out.println("Diferentes");
		}	
	}
}




// 4- Se acontecer dos funcionarios possuirem os mesmos valores nos atributos, a sua saída será apresentada como "Igual", a não ser pela função aumento.


//5- A condição if passará a não ser mais necessária, pois os funcionários 1 e 2 sempre serão iguais. Assim será sempre apresentada a mensagem "Iguais".
